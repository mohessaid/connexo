﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;


namespace TPRO
{
    public class GraphNonOrienter
    {
        // Declaration des variable necessaire pour designer un graph non orienter
        private List<UInt16> matrice; // Le variable qui represent la matrice d'ajacence
        private UInt16 nbrSommets; // Le variable qui represent le nombre des sommets
        private List<List<UInt16>> elementsConnexe; // Le variable qui represent les elements connexe

        // Default constructor
        public GraphNonOrienter(UInt16 nbrSommets)
        {
            Matrice = new List<UInt16>();
            elementsConnexe = new List<List<UInt16>>();
            NbrSommets = nbrSommets;
            CreateMatrice();
        }


        // Les proprieter qui less l'utilisateur modifier les variable marquer Private
        // La proprieter qui afficher la matrice d'ajacense
        public List<UInt16> Matrice
        {
            get { return matrice; }
            set { matrice = value; }
        }

        // La proprieter qui afficher le nombre des sommets et le modifier
        public UInt16 NbrSommets
        {
            get { return nbrSommets; }
            set { nbrSommets = value; }
        }

        // La proprieter qui afficher l'element connexe
        public List<List<UInt16>> ElementsConnexe
        {
            get { return elementsConnexe; }
        }

        /* 
           Les differents method, fonctions et procedure qui font des operation
           sur les differents variable de graph.... 
        */

        // Creation de la matrice
        private void CreateMatrice()
        {
            for (int i = 0, c = (nbrSommets * nbrSommets); i < c; i++)
            {
                matrice.Add(0);
            }
        }

        // Trouver les elements connexes
        public void CConnexe()
        {
            elementsConnexe.Add(new List<UInt16>() { 0 });

            UInt16 finElement = (UInt16)(nbrSommets - 1);
            UInt16 indexElementConnexe = 0;
            List<UInt16> tousSommet = new List<UInt16>() { 0 };
            while (finElement > 0)
            {
                UInt16 c = (UInt16)elementsConnexe.ElementAt<List<UInt16>>(indexElementConnexe).Count();
                UInt16 i = 0;
                UInt16 i1;
                while (i < c && finElement != 0)
                {
                    i1 = (UInt16)(elementsConnexe.ElementAt<List<UInt16>>(indexElementConnexe).ElementAt<UInt16>(i) * nbrSommets);
                    for (UInt16 j = 0; j < nbrSommets; j++)
                    {
                        if (matrice.ElementAt<UInt16>(i1 + j) == 1)
                        {
                            // Test if the element i1 is already in the list or not
                            Boolean exist = false;
                            foreach (UInt16 condition in tousSommet)
                            {
                                if (j == condition)
                                {
                                    exist = true;
                                    break;
                                }
                            }

                            if (!exist)
                            {
                                finElement--;
                                elementsConnexe.ElementAt<List<UInt16>>(indexElementConnexe).Add(j);
                                tousSommet.Add(j);
                                tousSommet.Sort();
                                elementsConnexe.ElementAt<List<UInt16>>(indexElementConnexe).Sort();

                            } // End if

                        }
                    }
                    i++;
                    c = (UInt16)elementsConnexe.ElementAt<List<UInt16>>(indexElementConnexe).Count();
                }

                UInt16 j1 = tousSommet.ElementAt<UInt16>(0);
                Boolean exist1 = true;

                while (exist1 && (j1 < nbrSommets) && (finElement > 0))
                {
                    foreach (UInt16 condition in tousSommet)
                    {
                        if (j1 == condition)
                        {
                            j1++;
                        }
                        else
                        {
                            exist1 = false;
                            break;
                        }

                    }

                }
                if (finElement > 1)
                {
                    indexElementConnexe++;
                    elementsConnexe.Add(new List<UInt16>() { j1 });
                    tousSommet.Add(j1);
                    tousSommet.Sort();
                    finElement--;
                }
                else if (finElement == 1)
                {
                    indexElementConnexe++;
                    elementsConnexe.Add(new List<UInt16>() { j1 });
                    tousSommet.Add(j1);
                    tousSommet.Sort();
                    finElement--;
                }

                

            } // Fin While

        } // Fin Function

    }

}
