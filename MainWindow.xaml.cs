﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;

namespace TPRO
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Variable pour enrigistrer les graphe non orienter
        List<Label> graphSommet;
        List<Line> graphArret;
        List<List<Label>> graphSommetCC;
        List<List<Line>> graphArretCC;
        List<Label> graphReduitSommetCC;
        List<Line> graphReduitArretCC;

        // Variable pour enrigistrer les graphe orienter
        List<Label> graphSommetGO;
        List<ArrowLine> graphArretGO;
        List<List<Label>> graphSommetCFC;
        List<List<ArrowLine>> graphArretCFC;
        List<Label> graphReduitSommetCFC;
        List<ArrowLine> graphReduitArretCFC;

        GraphNonOrienter gNO; // Le variable qui represente un graphe non orienter
        GraphOrienter gO; // le variable qui represente un graphe orienter
        List<Grid> interfaceOrder;
        Boolean typeDeGraph; // Varier avec le choix de l'utilisateur
        String affMatrice; // Le variable qui affiche la matrice dans l'interface
        List<UInt16> matrice; // Le variable qui occupe de modification de la matrice
        UInt16 nbrCC; // Le variable qui design l'element connexe afficher

        List<UInt16> zoomState;
        List<Double> demansionGrapheHolder;
        List<Double> demansionGrapheHolderConnexe;
        List<Double> demansionGrapheHolderReduit;

        public MainWindow()
        {
            InitializeComponent();


            interfaceOrder = new List<Grid>() { BackgroundImage, DailogBox, NouveauGraphe, MatriceInterface, GrapheInterface };
            affMatrice = "";
            zoomState = new List<UInt16>() { 0, 0, 0 };
        }

        #region Gestionnaire des evenment de l'application

        #region Gestion des evenment de Menu

        // Nouveau Graphe
        private void nouveauGraphe_Click(object sender, EventArgs e)
        {
            if (gNO == null && gO == null)
            {
                DailogBox.SetValue(Panel.ZIndexProperty, 4);
                BackgroundImage.SetValue(Panel.ZIndexProperty, 3);
            }
            else 
            {
                NouveauGraphe.SetValue(Panel.ZIndexProperty, 4);
            }
        }

        private void non_Click(object sender, EventArgs e)
        {
            NouveauGraphe.SetValue(Panel.ZIndexProperty, 1);
        }
        private void oui_Click(object sender, EventArgs e)
        {
            DailogBox.SetValue(Panel.ZIndexProperty, 4);
            NouveauGraphe.SetValue(Panel.ZIndexProperty, 1);
        }

        // Exit Application
        private void exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #endregion

        #region Traitement des donnes qui est entrer par l'utilisateur

        // Traitement de changement de text dans le text box des donnes
        private void textChangedEventHandler(object sender, TextChangedEventArgs args)
        {
            TextBox input = sender as TextBox;
            UInt16 i;
            if (input.Text == "")
            {
                input.Background = Brushes.White;
                input.Foreground = Brushes.Black;
                ErrorDonnees.Content = "Il faut entree un nombre";
                OkDB.IsEnabled = false;
            }
            else
            {
                try
                {
                    i = Convert.ToUInt16(input.Text);
                }
                catch (FormatException exception)
                {
                    input.BorderBrush = Brushes.Red;
                    input.Background = Brushes.Red;
                    input.Foreground = Brushes.White;
                    ErrorDonnees.Content = "Que des Nombre!";
                    OkDB.IsEnabled = false;

                }
                catch (OverflowException exception)
                {
                    input.BorderBrush = Brushes.Violet;
                    input.Background = Brushes.Violet;
                    input.Foreground = Brushes.White;
                    ErrorDonnees.Content = "Un nombre tres grand";
                    OkDB.IsEnabled = false;

                }
                finally
                {
                    if (UInt16.TryParse(input.Text, out i))
                    {
                        input.Background = Brushes.White;
                        input.Foreground = Brushes.Black;
                        ErrorDonnees.Content = "Trés bon!";
                        OkDB.IsEnabled = true;
                    }
                }
            }
        }

        // Traitement de l'evenment de click sur le button Ok de DailogBox
        private void okDB_Click(object sender, RoutedEventArgs e)
        {

            // Validation des donneés saisir
            

            // Cree le graph base sur le choix d'utilisateur
            if (GORadioButton.IsChecked == true)
            {
                typeDeGraph = true;
                gO = new GraphOrienter(UInt16.Parse(nbrSommetsTextBox.Text));
                
                // Affichage de matrice sur l'interface de matrice d'adjacence
                matrice = gO.Matrice;
                ComposeMatriceString();
                matriceTextBox.Text = affMatrice;
                affMatrice = "";
                AffColRow(gO.NbrSommets);
                elementList();
            }
            else
            {
                typeDeGraph = false;
                gNO = new GraphNonOrienter(UInt16.Parse(nbrSommetsTextBox.Text));

                // Affichage de matrice sur l'interface de matrice d'adjacence
                matrice = gNO.Matrice;
                ComposeMatriceString();
                matriceTextBox.Text = affMatrice;
                affMatrice = "";
                AffColRow(gNO.NbrSommets);
                elementList();
            }

            // Mettez le dialog box en arrierre
             DailogBox.SetValue(Panel.ZIndexProperty, 0);
             BackgroundImage.SetValue(Panel.ZIndexProperty, 1);
             MatriceInterface.SetValue(Panel.ZIndexProperty, 3);
            

        }

        // Traitement de l'evenment de click sur le button Cancel de DailogBox
        private void cancelDB_Click(object sender, RoutedEventArgs e)
        {
            DailogBox.SetValue(Panel.ZIndexProperty, 0);
        }

        #endregion


        // La procédure qui Laod Une Random Matrice 
        private void randomMatriceNO(ref List<UInt16> matrice)
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            Random valeur = new Random();
            int x = 0;
            int j = 1;
            while (x < numDesSomments)
            {
                for (int i = (x * numDesSomments) + j; i < ((j * numDesSomments)); i++)
                {
                    matrice[i] = Convert.ToUInt16(valeur.Next(0, 2)); ;
                }
                x++; j++;
            }
        }

        private void randomMatriceO(ref List<UInt16> matrice)
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            Random valeur = new Random();
            for (int i = 0; i < numDesSomments * numDesSomments; i++)
            {
                UInt16 x = Convert.ToUInt16(valeur.Next(0, 2));
                matrice[i] = x;
            }
            Diagonale(ref matrice);
        }

        private void symetrique(ref List<int> matrice)
        {
            int numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            Random valeur = new Random();
            int x = 0;
            while (x < numDesSomments)
            {
                for (int j = 0, i = (x * numDesSomments) + j; i < matrice.Count+1; i++, j++)
                {

                    matrice[i] = Convert.ToUInt16(valeur.Next(0, 2)); ;
                }
                x++;
            }

        }
        #region Traitement de la Matrice d'adjacence

        private void LoadMatrice_Click(object sender, EventArgs e)
        {
            if (typeDeGraph)
            {
                randomMatriceO(ref matrice);
                ComposeMatriceString();
                matriceTextBox.Text = affMatrice;
                affMatrice = "";
            }
            else
            {
                randomMatriceNO(ref matrice);
                ComposeMatriceString();
                matriceTextBox.Text = affMatrice;
                affMatrice = "";
            }
        }

        // Validation des changement sur la matrice d'adjacenece
        // Valider les changement sur la matrice d'adjacence
        private void ValiderLesChangement_Click(object sender, RoutedEventArgs e)
        {
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));

            if (typeDeGraph)
            {
                gO.Matrice = matrice;
                gO.NbrSommets = numDesSomments;
                gO.CFConnexe();
                ComposanteConnexeInterface.Header = "Composante Fortement Connexe";

                // Designer le Graphe
                graphSommetGO = new List<Label>();
                graphArretGO = new List<ArrowLine>();

                OperationSurGraphe.SommetArretePositionGO(matrice, ref graphArretGO, ref graphSommetGO);

                graphBoard.SetValue(Canvas.HeightProperty, Canvas.GetLeft(graphSommetGO.ElementAt<Label>(0)) * 2);
                graphBoard.SetValue(Canvas.WidthProperty, Canvas.GetLeft(graphSommetGO.ElementAt<Label>(0)) * 2);

                OperationSurGraphe.StylerSommets(ref graphSommetGO, "sommetTemplate");

                foreach (ArrowLine l in graphArretGO)
                    graphBoard.Children.Add(l);

                foreach (Label l in graphSommetGO)
                    graphBoard.Children.Add(l);

                // Designer les Composante Connexe
                graphSommetCFC = new List<List<Label>>();
                graphArretCFC = new List<List<ArrowLine>>();

                for (UInt16 i = 0, c = Convert.ToUInt16(gO.ElementsConnexe.Count); i < c; i++)
                {
                    List<UInt16> matriceCFC = new List<UInt16>();
                    List<ArrowLine> gACFC = new List<ArrowLine>();
                    List<Label> gSCFC = new List<Label>();

                    if (gO.ElementsConnexe != null)
                    {
                        OperationSurComposantConnexe.CCMatrice(matrice, gO.ElementsConnexe[i], ref matriceCFC);
                    }


                    OperationSurGraphe.SommetArretePositionGO(matriceCFC, ref gACFC, ref gSCFC);

                    cconnexeBoard.SetValue(Canvas.HeightProperty, Canvas.GetLeft(gSCFC.ElementAt<Label>(0)) * 2);
                    cconnexeBoard.SetValue(Canvas.WidthProperty, Canvas.GetLeft(gSCFC.ElementAt<Label>(0)) * 2);

                    OperationSurGraphe.StylerSommets(ref gSCFC, "sommetTemplate");

                    for (UInt16 j = 0, j1 = Convert.ToUInt16(gO.ElementsConnexe[i].Count); j < j1; j++)
                    {
                        gSCFC[j].Content = Convert.ToString(gO.ElementsConnexe[i].ElementAt<UInt16>(j));
                    }

                    graphArretCFC.Add(gACFC);
                    graphSommetCFC.Add(gSCFC);


                }

                foreach (ArrowLine l in graphArretCFC[0])
                    cconnexeBoard.Children.Add(l);

                foreach (Label l in graphSommetCFC[0])
                    cconnexeBoard.Children.Add(l);

                // Designer le graphe Reduit
                graphReduitSommetCFC = new List<Label>();
                graphReduitArretCFC = new List<ArrowLine>();

                List<UInt16> matriceReduitCFC = new List<UInt16>();

                gO.GrapheReduit(ref matriceReduitCFC);

                OperationSurGraphe.SommetArretePositionGO(matriceReduitCFC, ref graphReduitArretCFC, ref graphReduitSommetCFC);

                GrapheReduitBoardCC.SetValue(Canvas.HeightProperty, Canvas.GetLeft(graphReduitSommetCFC.ElementAt<Label>(0)) * 2);
                GrapheReduitBoardCC.SetValue(Canvas.WidthProperty, Canvas.GetLeft(graphReduitSommetCFC.ElementAt<Label>(0)) * 2);

                OperationSurGraphe.StylerSommets(ref graphReduitSommetCFC, "sommetTemplate");

                for (UInt16 j = 0, j1 = Convert.ToUInt16(graphReduitSommetCFC.Count); j < j1; j++)
                {
                    graphReduitSommetCFC[j].Content = "CF" + Convert.ToString(j);
                }

                foreach (ArrowLine l in graphReduitArretCFC)
                    GrapheReduitBoardCC.Children.Add(l);

                foreach (Label l in graphReduitSommetCFC)
                    GrapheReduitBoardCC.Children.Add(l);
            }
            else
            {
                gNO.Matrice = matrice;
                gNO.NbrSommets = numDesSomments;
                gNO.CConnexe();
                ComposanteConnexeInterface.Header = "Composante Connexe";

                // Designer le Graphe
                graphSommet = new List<Label>();
                graphArret = new List<Line>();

                OperationSurGraphe.SommetArretePositionGNO(matrice, ref graphArret, ref graphSommet);

                graphBoard.SetValue(Canvas.HeightProperty, Canvas.GetLeft(graphSommet.ElementAt<Label>(0)) * 2);
                graphBoard.SetValue(Canvas.WidthProperty, Canvas.GetLeft(graphSommet.ElementAt<Label>(0)) * 2);

                OperationSurGraphe.StylerSommets(ref graphSommet, "sommetTemplate");

                foreach (Line l in graphArret)
                    graphBoard.Children.Add(l);

                foreach (Label l in graphSommet)
                    graphBoard.Children.Add(l);


                // Designer les Composante Connexe
                graphSommetCC = new List<List<Label>>();
                graphArretCC = new List<List<Line>>();

                for (UInt16 i = 0, c = Convert.ToUInt16(gNO.ElementsConnexe.Count); i < c; i++)
                {
                    List<UInt16> matriceCC = new List<UInt16>();
                    List<Line> gACC = new List<Line>();
                    List<Label> gSCC = new List<Label>();

                    if (gNO.ElementsConnexe != null)
                    {
                        OperationSurComposantConnexe.CCMatrice(matrice, gNO.ElementsConnexe[i], ref matriceCC);
                    }

                    
                    OperationSurGraphe.SommetArretePositionGNO(matriceCC, ref gACC, ref gSCC);

                    cconnexeBoard.SetValue(Canvas.HeightProperty, Canvas.GetLeft(gSCC.ElementAt<Label>(0)) * 2);
                    cconnexeBoard.SetValue(Canvas.WidthProperty, Canvas.GetLeft(gSCC.ElementAt<Label>(0)) * 2);

                    OperationSurGraphe.StylerSommets(ref gSCC, "sommetTemplate");

                    for(UInt16 j = 0, j1 = Convert.ToUInt16(gNO.ElementsConnexe[i].Count); j < j1; j++)
                    {
                        gSCC[j].Content = Convert.ToString(gNO.ElementsConnexe[i].ElementAt<UInt16>(j));
                    }

                    graphArretCC.Add(gACC);
                    graphSommetCC.Add(gSCC);


                }

                foreach (Line l in graphArretCC[0])
                    cconnexeBoard.Children.Add(l);

                foreach (Label l in graphSommetCC[0])
                    cconnexeBoard.Children.Add(l);

                // Designer le graphe Reduit
                graphReduitSommetCC = new List<Label>();
                graphReduitArretCC = new List<Line>();
                
                List<UInt16> matriceReduitCC = new List<UInt16>();

                for (UInt16 i = 0, c = Convert.ToUInt16(Math.Pow(gNO.ElementsConnexe.Count, 2)); i < c; i++)
                    matriceReduitCC.Add(0);

                OperationSurGraphe.SommetArretePositionGNO(matriceReduitCC, ref graphReduitArretCC, ref graphReduitSommetCC);

                GrapheReduitBoardCC.SetValue(Canvas.HeightProperty, Canvas.GetLeft(graphReduitSommetCC.ElementAt<Label>(0)) * 2);
                GrapheReduitBoardCC.SetValue(Canvas.WidthProperty, Canvas.GetLeft(graphReduitSommetCC.ElementAt<Label>(0)) * 2);

                OperationSurGraphe.StylerSommets(ref graphReduitSommetCC, "sommetTemplate");

                for (UInt16 j = 0, j1 = Convert.ToUInt16(graphReduitSommetCC.Count); j < j1; j++)
                {
                    graphReduitSommetCC[j].Content = "C" + Convert.ToString(j);
                }

                foreach (Line l in graphReduitArretCC)
                    GrapheReduitBoardCC.Children.Add(l);

                foreach (Label l in graphReduitSommetCC)
                    GrapheReduitBoardCC.Children.Add(l);

            }
            
            MatriceInterface.SetValue(Panel.ZIndexProperty, 3);
            GrapheInterface.SetValue(Panel.ZIndexProperty,4);
        }


        #region Tous les procedure qui creé la matrice et ajouter ou supprimer des Sommets

        // La methode qui traiter l'evenement de clique sur le button +
        private void btnAddMI_Click(object sender, EventArgs e)
        {
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            AddSomments();
            ComposeMatriceString();
            matriceTextBox.Text = affMatrice;
            affMatrice = "";
            rowIndices.Text += numDesSomments + "\n\n" + " ";
            colIndices.Text += numDesSomments + "\t";
            elementList();

        }

        // La methode qui traiter l'evenement de clique sur le button -
        private void btnRemoveMI_Click(object sender, EventArgs e)
        {

            SuppSommets();
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            ComposeMatriceString();
            matriceTextBox.Text = affMatrice;
            affMatrice = "";
            AffColRow(numDesSomments);
            elementList();
        }

        // Composer la matrice sous un chaine bien former
        private void ComposeMatriceString()
        {
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            affMatrice = "";
            for (int i = 0, c = matrice.Count; i < c; i += numDesSomments)
            {
                for (UInt16 j = 0; j < numDesSomments; j++)
                {
                    affMatrice += matrice.ElementAt<UInt16>(j + i) + "\t";
                }
                affMatrice += "\n\n";
            }
        }

        // Ajouter un somments
        private void AddSomments()
        {
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            List<UInt16> matriceClone = new List<UInt16>();

            for (int i = 0, c = matrice.Count; i < c; i += numDesSomments)
            {
                for (UInt16 j = 0; j < numDesSomments; j++)
                {
                    matriceClone.Add(matrice.ElementAt<UInt16>(j + i));
                }
                matriceClone.Add(0);
            }

            for (int i = 0, c = (numDesSomments + 1); i < c; i++)
            {
                matriceClone.Add(0);
            }

            matrice = matriceClone;
        }

        // Supprimer un Sommets
        private void SuppSommets()
        {
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            List<UInt16> matriceClone = new List<UInt16>();

            for (int i = 0, c = (matrice.Count - numDesSomments); i < c; i += numDesSomments)
            {
                for (int j = 0, cj = (numDesSomments - 1); j < cj; j++)
                {
                    matriceClone.Add(matrice.ElementAt<UInt16>(j + i));
                }
            }
            matrice = matriceClone;
        }

        // La methode qui occupe de la affichage des indices dans la grid

        private void AffColRow(UInt16 prm1)
        {
            colIndices.Text = " ";
            rowIndices.Text = " ";
            for (UInt16 i = 0; i < prm1; i++)
            {
                rowIndices.Text += i + "\n\n" + " ";
                colIndices.Text += i + "\t";
            }
        }

        #endregion

        #region Tous Les procédure pour remples la matrice adjacance dans le cas ou le graphe est non orienté

        // la procedure qui metre tous les éléments de la diagonale à 0
        private void Diagonale(ref List<UInt16> matrice)
        {
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            for (int i = 0; i < matrice.Count; i += (numDesSomments + 1))
            {
                matrice[i] = 0;
            }
        }

        // procedure qui affecte la valeur 1 à la matrice
        private void RemplesM1(ref List<UInt16> matrice, UInt16 x, UInt16 y)
        {
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            
            if (radioBtnCellule.IsChecked == true)
            {
                matrice[(x * numDesSomments) + y] = 1;
                matrice[(y * numDesSomments) + x] = 1;
                Diagonale(ref matrice);
            }
            else if (radioBtnLine.IsChecked == true)
            {
                for (int i = x * numDesSomments; i < numDesSomments * (x + 1); i++)
                {
                    matrice[i] = 1;
                }
                for (UInt16 i = x, j = x; i < numDesSomments * numDesSomments; j++, i += numDesSomments)
                {
                    if (j == i)
                    {
                        matrice[i] = 0;
                    }
                    matrice[i] = 1;
                }
                Diagonale(ref matrice);
            }
            else if (radioBtnColone.IsChecked == true)
            {
                for (UInt16 i = y; i < numDesSomments * numDesSomments; i += numDesSomments)
                {
                    matrice[i] = 1;
                }
                for (int i = y * numDesSomments; i < numDesSomments * (y + 1); i++)
                {
                    matrice[i] = 1;
                }
                Diagonale(ref matrice);
            }
        }

        // procedure qui affecte la valeur 0 à la matrice
        private void RemplesM0(ref List<UInt16> matrice, UInt16 x, UInt16 y)
        {
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            
            if (radioBtnCellule.IsChecked == true)  // rempler la cellule qui vous choiseé
            {
                matrice[(x * numDesSomments) + y] = 0;
                matrice[(y * numDesSomments) + x] = 0;
                Diagonale(ref matrice);
            }
            else if (radioBtnLine.IsChecked == true) // rempler ler lignes qui vous choiseé
            {
                for (int i = x * numDesSomments; i < numDesSomments * (x + 1); i++)
                {
                    matrice[i] = 0;
                }
                for (UInt16 i = x; i < numDesSomments * numDesSomments; i += numDesSomments)
                {
                    matrice[i] = 0;
                }
                Diagonale(ref matrice);
            }
            else if (radioBtnColone.IsChecked == true) // rempler les colones qui voux choiseé
            {
                for (UInt16 i = y; i < numDesSomments * numDesSomments; i += numDesSomments)
                {
                    matrice[i] = 0;
                }
                for (int i = y * numDesSomments; i < numDesSomments * (y + 1); i++)
                {
                    matrice[i] = 0;
                }
                Diagonale(ref matrice);
            }
        }

        //button pour rempler la matrice à 1
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UInt16 x = 0;
                UInt16 y = 0;

                if (radioBtnLine.IsChecked == true || radioBtnCellule.IsChecked == true)
                { 
                    x = Convert.ToUInt16(textboxline.Text); 
                }
                if (radioBtnCellule.IsChecked == true || radioBtnColone.IsChecked == true)
                {
                    y = Convert.ToUInt16(textboxcolone.Text);
                }

                if (typeDeGraph)
                    RemplesM1orienté(ref matrice, x, y);
                else
                    RemplesM1(ref matrice, x, y);

            }
            catch (FormatException exception)
            {
                if (textboxline.Text == "")
                    MessageBox.Show("Il faut selectionner un ligne", "Ligne non exiter",
                        MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.None);
                else if (textboxcolone.Text == "")
                    MessageBox.Show("Il faut selectionner une colone", "Colone non exiter",
                        MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.None);
            }
            
            ComposeMatriceString();
            matriceTextBox.Text = affMatrice;
            affMatrice = "";
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            AffColRow(numDesSomments);
        }

        //button pour rempler la matrice à 0
        private void Button_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                UInt16 x = Convert.ToUInt16(textboxline.Text);
                UInt16 y = Convert.ToUInt16(textboxcolone.Text);

                if (radioBtnLine.IsChecked == true || radioBtnCellule.IsChecked == true)
                {
                    x = Convert.ToUInt16(textboxline.Text);
                }
                if (radioBtnCellule.IsChecked == true || radioBtnColone.IsChecked == true)
                {
                    y = Convert.ToUInt16(textboxcolone.Text);
                }

                if (typeDeGraph)
                    RemplesM0orienté(ref matrice, x, y);
                else
                    RemplesM0(ref matrice, x, y);
            }
            catch (FormatException exception)
            {
                if (textboxcolone.Text == null)
                    MessageBox.Show("Il faut selectionner une colone", "Colone non exiter",
                        MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.None);
                else if (textboxline.Text == null)
                    MessageBox.Show("Il faut selectionner un ligne", "Ligne non exiter",
                        MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.None);
            }

            ComposeMatriceString();
            matriceTextBox.Text = affMatrice;
            affMatrice = "";
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            AffColRow(numDesSomments);
        }

        #endregion

        #region dans le cas ou le graphe est orienté

        private void RemplesM1orienté(ref List<UInt16> matrice, UInt16 x, UInt16 y)
        {
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            if (radioBtnCellule.IsChecked == true)
            {
                matrice[(x * numDesSomments) + y] = 1;
            }
            else if (radioBtnLine.IsChecked == true)
            {
                for (int i = x * numDesSomments; i < numDesSomments * (x + 1); i++)
                {
                    matrice[i] = 1;
                }
            }
            else if (radioBtnColone.IsChecked == true)
            {
                for (UInt16 i = y; i < numDesSomments * numDesSomments; i += numDesSomments)
                {
                    matrice[i] = 1;
                }
            }
            Diagonale(ref matrice);
        }

        private void RemplesM0orienté(ref List<UInt16> matrice, UInt16 x, UInt16 y)
        {
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            
            if (radioBtnCellule.IsChecked == true)  // rempler la cellule qui vous choiseé
            {
                matrice[(x * numDesSomments) + y] = 0;
                matrice[(y * numDesSomments) + x] = 0;
            }
            else if (radioBtnLine.IsChecked == true) // rempler ler lignes qui vous choiseé
            {
                for (int i = x * numDesSomments; i < numDesSomments * (x + 1); i++)
                {
                    matrice[i] = 0;
                }
            }
            else if (radioBtnColone.IsChecked == true) // rempler les colones qui voux choiseé
            {
                for (UInt16 i = y; i < numDesSomments * numDesSomments; i += numDesSomments)
                {
                    matrice[i] = 0;
                }
            }
            Diagonale(ref matrice);
        }

        // Pour remplir les combox
        private void elementList()
        {
            UInt16 numDesSomments = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            textboxcolone.Items.Clear();
            textboxline.Items.Clear();
            for (UInt16 i = 0; i < numDesSomments; i++)
            {
                textboxline.Items.Add(i);
                textboxcolone.Items.Add(i);
            }
        }

        #endregion

  

        #endregion

        #region Traitement de Graphe
        private void zoomGraphe_ValueChanged(object sender, EventArgs e)
        {
            if (zoomState != null)
            {
                if (zoomState[0] == 0)
                {
                    zoomState[0] = 1;
                    demansionGrapheHolder = new List<Double>()
                {
                    GrapheHolder.ActualHeight,
                    GrapheHolder.ActualWidth
                };



                }
                else
                {
                    GrapheHolder.Height = demansionGrapheHolder[0] * ZoomGraphe.Value;
                    GrapheHolder.Width = demansionGrapheHolder[1] * ZoomGraphe.Value;

                }
            }
        }

        // Pour zoomer les elements connexes
        private void zoomGrapheConnexe_ValueChanged(object sender, EventArgs e)
        {
            if (zoomState != null)
            {
                if (zoomState[1] == 0)
                {
                    zoomState[1] = 1;
                    demansionGrapheHolderConnexe = new List<Double>()
                {
                    GrapheHolderConnexe.ActualHeight,
                    GrapheHolderConnexe.ActualWidth
                };



                }
                else
                {
                    GrapheHolderConnexe.Height = demansionGrapheHolderConnexe[0] * ZoomGrapheConnexe.Value;
                    GrapheHolderConnexe.Width = demansionGrapheHolderConnexe[1] * ZoomGrapheConnexe.Value;

                }
            }
        }

        private void zoomGrapheReduit_ValueChanged(object sender, EventArgs e)
        {
            if (zoomState != null)
            {
                if (zoomState[2] == 0)
                {
                    zoomState[2] = 1;
                    demansionGrapheHolderReduit = new List<Double>()
                {
                    GrapheHolderReduit.ActualHeight,
                    GrapheHolderReduit.ActualWidth
                };



                }
                else
                {
                    GrapheHolderReduit.Height = demansionGrapheHolderReduit[0] * ZoomGrapheReduit.Value;
                    GrapheHolderReduit.Width = demansionGrapheHolderReduit[1] * ZoomGrapheReduit.Value;

                }
            }
        }
        #endregion

        #region Traitement des Composante Connexe

        private void NextCC_Click(object sender, EventArgs e)
        {
            if (!typeDeGraph)
            {
                if ((nbrCC + 1) < graphSommetCC.Count)
                {
                    cconnexeBoard.Children.Clear();
                    nbrCC++;
                    foreach (Line l in graphArretCC[nbrCC])
                        cconnexeBoard.Children.Add(l);
                    foreach (Label l in graphSommetCC[nbrCC])
                        cconnexeBoard.Children.Add(l);
                }
            }
            else
            {
                if ((nbrCC + 1) < graphSommetCFC.Count)
                {
                    cconnexeBoard.Children.Clear();
                    nbrCC++;
                    foreach (ArrowLine l in graphArretCFC[nbrCC])
                        cconnexeBoard.Children.Add(l);
                    foreach (Label l in graphSommetCFC[nbrCC])
                        cconnexeBoard.Children.Add(l);
                }
            }

        }

        private void PrevCC_Click(object sender, EventArgs e)
        {
            if (!typeDeGraph)
            {
                if ((nbrCC - 1) >= 0)
                {
                    cconnexeBoard.Children.Clear();
                    nbrCC--;
                    foreach (Line l in graphArretCC[nbrCC])
                        cconnexeBoard.Children.Add(l);
                    foreach (Label l in graphSommetCC[nbrCC])
                        cconnexeBoard.Children.Add(l);
                }
            }
            else
            {
                if ((nbrCC - 1) >= 0)
                {
                    cconnexeBoard.Children.Clear();
                    nbrCC--;
                    foreach (ArrowLine l in graphArretCFC[nbrCC])
                        cconnexeBoard.Children.Add(l);
                    foreach (Label l in graphSommetCFC[nbrCC])
                        cconnexeBoard.Children.Add(l);
                }
            }
        }

        #endregion

        #region Traitement des Composante Fortement Connexe

        #endregion

        #region Traitement de Graphe Reduit

        #endregion

        
    }
}
