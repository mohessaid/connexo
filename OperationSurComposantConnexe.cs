﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TPRO
{
    static class OperationSurComposantConnexe
    {
        public static void CCMatrice(List<UInt16> matrice, List<UInt16> composanteConnexe, ref List<UInt16> ccmatrice)
        {
            UInt16 nbrSommets = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            UInt16 nbrConnexe = Convert.ToUInt16( composanteConnexe.Count);
            for (UInt16 i1 = 0, i2 = 0; i1 < nbrSommets; i1++, i2 += nbrSommets)
            {
                Boolean contStop = false;
                for (UInt16 j = 0; j < nbrConnexe; j++)
                {
                    if (i1 == composanteConnexe[j])
                    {
                        contStop = true;
                        break;
                    }
                }
                if (contStop)
                {
                    for (UInt16 j2 = 0; j2 < nbrSommets; j2++)
                    {
                        UInt16 elem = 0;
                        UInt16 i = composanteConnexe[elem];

                        while ((i != j2) && (elem < nbrConnexe))
                        {
                            i = composanteConnexe.ElementAt<UInt16>(elem++);
                        }

                        if (i == j2)
                            ccmatrice.Add(matrice.ElementAt<UInt16>(i2 + j2));
                    }
                }
               
            }
        }
    }
}
