﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TPRO
{
    static class OperationSurLesMatrices
    {
        // Creation de la tmatrice 
        public static void CreateTMatrice(List<UInt16> matrice,ref List<UInt16> matriceT)
        {
            UInt16 nbrSommets = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            UInt16 c = Convert.ToUInt16(matrice.Count);
            for (UInt16 i = 0; i < nbrSommets; i++)
            {
                for (UInt16 j = 0; j < c; j += nbrSommets)
                {
                    matriceT.Add(matrice.ElementAt<UInt16>(i + j));
                }
            }
        }
    }
}
