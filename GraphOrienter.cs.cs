﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TPRO
{
    public class GraphOrienter
    {
        // Declaration des variable necessaire pour designer un graph non orienter
        private List<UInt16> matrice; // Le variable qui represent la matrice d'ajacence
        private List<UInt16> tmatrice; // Le variable qui represent le transposer de lat matrice d'ajacence
        private UInt16 nbrSommets; // Le variable qui represent le nombre des sommets
        private List<List<UInt16>> elementsFConnexe; // Le variable qui represent les elements connexe

        // Default constructor
        public GraphOrienter(UInt16 nbrSommets)
        {
            Matrice = new List<UInt16>();
            elementsFConnexe = new List<List<UInt16>>();
            NbrSommets = nbrSommets;
            CreateMatrice();
            tmatrice = new List<UInt16>();
        }


        // Les proprieter qui less l'utilisateur modifier les variable marquer Private
        // La proprieter qui afficher la matrice d'ajacense
        public List<UInt16> Matrice
        {
            get { return matrice; }
            set { matrice = value; }
        }

        // La proprieter qui afficher le nombre des sommets et le modifier
        public UInt16 NbrSommets
        {
            get { return nbrSommets; }
            set { nbrSommets = value; }
        }

        // La proprieter qui afficher les elements connexe
        public List<List<UInt16>> ElementsConnexe
        {
            get { return elementsFConnexe; }
        }

        /* 
           Les differents method, fonctions et procedure qui font des operation
           sur les differents variable de graph.... 
        */

        // Creation de la matrice
        private void CreateMatrice()
        {
            for (int i = 0, c = (nbrSommets * nbrSommets); i < c; i++)
            {
                matrice.Add(0);
            }
        }

        // Trouver les elements connexes
        public void CFConnexe()
        {
            OperationSurLesMatrices.CreateTMatrice(matrice, ref tmatrice);

            elementsFConnexe.Add(new List<UInt16>());
            List<UInt16> elementsFConnexeM = new List<UInt16>() { 0 };
            List<UInt16> elementsFConnexeT = new List<UInt16>() { 0 };
            

            UInt16 finElement = (UInt16)(nbrSommets);
            UInt16 finElementM = (UInt16)(nbrSommets - 1);
            UInt16 finElementT = (UInt16)(nbrSommets - 1);

            UInt16 indexElementConnexe = 0;

            List<UInt16> tousSommet = new List<UInt16>();
            List<UInt16> tousSommetM = new List<UInt16>() { 0 };
            List<UInt16> tousSommetT = new List<UInt16>() { 0 };

            while (finElement > 0)
            {
                UInt16 c = (UInt16)elementsFConnexe.ElementAt<List<UInt16>>(indexElementConnexe).Count();
                UInt16 cM = (UInt16)elementsFConnexeM.Count();
                UInt16 cT = (UInt16)elementsFConnexeT.Count();


                #region Pour Trouver le composante connexe de Matrice normal
                UInt16 iM = 0;
                UInt16 i1M;
                while (iM < cM && finElementM != 0)
                {
                    i1M = (UInt16)(elementsFConnexeM.ElementAt<UInt16>(iM) * nbrSommets);
                    for (UInt16 jM = 0; jM < nbrSommets; jM++)
                    {
                        if (matrice.ElementAt<UInt16>(i1M + jM) == 1)
                        {
                            // Test if the element i1 is already in the list or not
                            Boolean exist = false;
                            foreach (UInt16 condition in tousSommetM)
                            {
                                if (jM == condition)
                                {
                                    exist = true;
                                    break;
                                }
                            }

                            if (!exist)
                            {
                                finElementM--;
                                elementsFConnexeM.Add(jM);
                                tousSommetM.Add(jM);
                                tousSommetM.Sort();
                                elementsFConnexeM.Sort();

                            } // End if

                        }
                    }
                    iM++;
                    cM = (UInt16)elementsFConnexeM.Count();
                }

                #endregion

                #region Pour Trouver le composante connexe de Matrice Transposer
                UInt16 iT = 0;
                UInt16 i1T;
                while (iT < cT && finElementT != 0)
                {
                    i1T = (UInt16)(elementsFConnexeT.ElementAt<UInt16>(iT) * nbrSommets);
                    for (UInt16 jT = 0; jT < nbrSommets; jT++)
                    {
                        if (tmatrice.ElementAt<UInt16>(i1T + jT) == 1)
                        {
                            // Test if the element i1 is already in the list or not
                            Boolean exist = false;
                            foreach (UInt16 condition in tousSommetT)
                            {
                                if (jT == condition)
                                {
                                    exist = true;
                                    break;
                                }
                            }

                            if (!exist)
                            {
                                finElementT--;
                                elementsFConnexeT.Add(jT);
                                tousSommetT.Add(jT);
                                tousSommetT.Sort();
                                elementsFConnexeT.Sort();

                            } // End if

                        }
                    }
                    iT++;
                    cT = (UInt16)elementsFConnexeT.Count();
                }

                #endregion


                if (elementsFConnexeM.Count <= elementsFConnexeT.Count)
                {
                    foreach (UInt16 k1 in elementsFConnexeM)
                    {
                        foreach (UInt16 k2 in elementsFConnexeT)
                        {

                            // Test if the element i1 is already in the list or not
                            Boolean exist = false;
                            foreach (UInt16 condition in tousSommet)
                            {
                                if (k1 == condition)
                                {
                                    exist = true;
                                    break;
                                }
                            }

                            if (!exist)
                            {
                                elementsFConnexe.ElementAt<List<UInt16>>(indexElementConnexe).Add(k1);
                                elementsFConnexe.ElementAt<List<UInt16>>(indexElementConnexe).Sort();
                                finElement--;
                                tousSommet.Add(k1);
                                tousSommet.Sort();
                            }
                        }  
                    }
                }
                else
                {
                    foreach (UInt16 k1 in elementsFConnexeT)
                    {
                        foreach (UInt16 k2 in elementsFConnexeM)
                        {
                            if (k1 == k2)
                            {
                                // Test if the element i1 is already in the list or not
                                Boolean exist = false;
                                foreach (UInt16 condition in tousSommet)
                                {
                                    if (k1 == condition)
                                    {
                                        exist = true;
                                        break;
                                    }
                                }

                                if (!exist)
                                {
                                    elementsFConnexe.ElementAt<List<UInt16>>(indexElementConnexe).Add(k1);
                                    elementsFConnexe.ElementAt<List<UInt16>>(indexElementConnexe).Sort();
                                    finElement--;
                                    tousSommet.Add(k1);
                                    tousSommet.Sort();
                                }
                            }
                            
                        }
                    }
                }

                UInt16 j1 = tousSommet.ElementAt<UInt16>(0);
                Boolean exist1 = true;

                while (exist1 && (j1 < nbrSommets) && (finElement > 0))
                {
                    foreach (UInt16 condition in tousSommet)
                    {
                        if (j1 == condition)
                        {
                            j1++;
                        }
                        else
                        {
                            exist1 = false;
                            break;
                        }

                    }

                }
                if (finElement > 1)
                {
                    indexElementConnexe++;
                    elementsFConnexe.Add(new List<UInt16>() { j1 });
                    tousSommet.Add(j1);
                    tousSommet.Sort();
                    finElement--;
                    tousSommetM.Clear();
                    tousSommetT.Clear();

                    foreach (UInt16 i in tousSommet)
                    {
                        tousSommetM.Add(i);
                        tousSommetT.Add(i);
                        finElementM = (UInt16)(nbrSommets - 1);
                        finElementT = (UInt16)(nbrSommets - 1);
                    }
                }
                else if (finElement == 1)
                {
                    indexElementConnexe++;
                    elementsFConnexe.Add(new List<UInt16>() { j1 });
                    tousSommet.Add(j1);
                    tousSommet.Sort();
                    finElement--;
                }

                

            } // Fin While

        } // Fin Function 
  
        // Trouve le Graphe Reduit
        public void GrapheReduit(ref List<UInt16> grapheReduitMatrice)
        {
            if (elementsFConnexe.Count > 0)
            {
                grapheReduitMatrice.Clear();
                for (UInt16 i = 0, c = Convert.ToUInt16(elementsFConnexe.Count); i < c; i++)
                {
                    UInt16 c1 = Convert.ToUInt16(elementsFConnexe[i].Count);

                    for (UInt16 j = 0; j < c; j++)
                    {
                        if (i == j)
                        {
                            grapheReduitMatrice.Add(0);
                        }
                        else
                        {
                            Boolean existe = false;
                            UInt16 c2 = Convert.ToUInt16(elementsFConnexe[j].Count);

                            // Parcourer les sommet de composant connexe en cours
                            for (UInt16 i1 = 0; i1 < c1; i1++)
                            {
                                // Parcourer la ligne de sommet de composant connexe 
                                // en cours sur la matrice principale

                                for (UInt16 i2 = 0, i3 = 0; i2 < nbrSommets; i2++, i3 += nbrSommets)
                                {
                                    if (i2 == elementsFConnexe[i].ElementAt<UInt16>(i1))
                                    {
                                        for (UInt16 i4 = 0; i4 < nbrSommets; i4++)
                                        {
                                            if (matrice[i3 + i4] == 1)
                                            {
                                                for (UInt16 i5 = 0; i5 < c2; i5++)
                                                {
                                                    if (elementsFConnexe[j].ElementAt<UInt16>(i5) == i4)
                                                    {
                                                        existe = true;
                                                        grapheReduitMatrice.Add(1);
                                                        break;
                                                    }
                                                }
                                            }

                                            if (existe)
                                                break;
                                        }
                                        if (existe)
                                            break;
                                    }

                                    if (existe)
                                        break;
                                }
                            }

                            if (!existe)
                                grapheReduitMatrice.Add(0);
                        }
                    }
                }
            }
        }

    }
}
