﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TPRO
{
    static class OperationSurGraphe
    {
        public static void SommetArretePositionGNO(List<UInt16> matrice, ref List<Line> arrets, ref List<Label> sommets)
        {
            UInt16 nbrSommets = Convert.ToUInt16(Math.Sqrt(matrice.Count));

            double angel = 360 / nbrSommets * (Math.PI / 180);
            double x1 = nbrSommets / 2 * 35, x2;
            double y1 = 0, y2;
            double size = (x1 * 2 + 200);

            double centerX = size / 2;
            double centerY = size / 2;

            for (int i = 0; i < nbrSommets; i++)
            {
                x2 = x1 * Math.Cos(angel) - y1 * Math.Sin(angel);
                y2 = x1 * Math.Sin(angel) + y1 * Math.Cos(angel);

                Label sommet = new Label();

                sommet.Content = i.ToString();
                sommet.VerticalAlignment = VerticalAlignment.Center;
                sommet.HorizontalAlignment = HorizontalAlignment.Center;
                sommet.FontSize = 15;
                sommet.FontWeight = FontWeights.UltraBold;
                sommet.Height = 50;
                sommet.Width = 50;
                sommet.BorderBrush = Brushes.Black;

                Canvas.SetTop(sommet, y1 + centerY);
                Canvas.SetLeft(sommet, x1 + centerX);
                x1 = x2; y1 = y2;

                sommets.Add(sommet);

            }

            for (int i1 = 0, i2 = 0; i1 < nbrSommets; i1++, i2 += nbrSommets)
            {
                for (int j1 = (i2 + i1), j2 = i1; j2 < nbrSommets; j1++, j2++)
                {

                    if (matrice.ElementAt<UInt16>(j1) == 1)
                    {
                        double xl1 = (double)sommets.ElementAt<Label>(i1).GetValue(Canvas.LeftProperty);
                        double yl1 = (double)sommets.ElementAt<Label>(i1).GetValue(Canvas.TopProperty);
                        double xl2 = (double)sommets.ElementAt<Label>(j2).GetValue(Canvas.LeftProperty);
                        double yl2 = (double)sommets.ElementAt<Label>(j2).GetValue(Canvas.TopProperty);
                        Line arret = new Line()
                        {
                            X1 = xl1 + 25,
                            X2 = xl2 + 25,
                            Y1 = yl1 + 25,
                            Y2 = yl2 + 25,
                            Stroke = Brushes.Black,
                            StrokeThickness = 3
                        };
                        arrets.Add(arret);

                    }
                }
            }
        }

        public static void SommetArretePositionGO(List<UInt16> matrice, ref List<ArrowLine> arrets, ref List<Label> sommets)
        {
            UInt16 nbrSommets = Convert.ToUInt16(Math.Sqrt(matrice.Count));
            List<UInt16> tmatrice = new List<UInt16>();
            OperationSurLesMatrices.CreateTMatrice(matrice, ref tmatrice);

            double angel = 360 / nbrSommets * (Math.PI / 180);
            double x1 = nbrSommets / 2 * 35, x2;
            double y1 = 0, y2;
            double size = (x1 * 2 + 200);

            double centerX = size / 2;
            double centerY = size / 2;

            for (int i = 0; i < nbrSommets; i++)
            {
                x2 = x1 * Math.Cos(angel) - y1 * Math.Sin(angel);
                y2 = x1 * Math.Sin(angel) + y1 * Math.Cos(angel);

                Label sommet = new Label();

                sommet.Content = i.ToString();
                sommet.VerticalAlignment = VerticalAlignment.Center;
                sommet.HorizontalAlignment = HorizontalAlignment.Center;
                sommet.FontSize = 15;
                sommet.FontWeight = FontWeights.UltraBold;
                sommet.Height = 50;
                sommet.Width = 50;
                sommet.BorderBrush = Brushes.Black;

                Canvas.SetTop(sommet, y1 + centerY);
                Canvas.SetLeft(sommet, x1 + centerX);
                x1 = x2; y1 = y2;

                sommets.Add(sommet);

            }

            for (int i1 = 0, i2 = 0; i1 < nbrSommets; i1++, i2 += nbrSommets)
            {
                for (int j1 = (i2 + i1), j2 = i1; j2 < nbrSommets; j1++, j2++)
                {

                    if (matrice.ElementAt<UInt16>(j1) == 1)
                    {
                        double xl1 = (double)sommets.ElementAt<Label>(i1).GetValue(Canvas.LeftProperty) + 25;
                        double yl1 = (double)sommets.ElementAt<Label>(i1).GetValue(Canvas.TopProperty) + 25;
                        double xl2 = (double)sommets.ElementAt<Label>(j2).GetValue(Canvas.LeftProperty) + 25;
                        double yl2 = (double)sommets.ElementAt<Label>(j2).GetValue(Canvas.TopProperty) + 25;

                        ArrowLine arret = new ArrowLine()
                        {
                            Height= size,
                            Width = size,                            
                            Stroke = Brushes.Black,
                            StrokeThickness = 3,
                            IsArrowheadClosed= true,
                            ArrowheadEnd = ArrowLine.ArrowheadEndEnum.End,
                            ArrowheadSizeX = 4,
                            ArrowheadSizeY = 3
                        };
                        if (tmatrice.ElementAt<UInt16>(j1) == 1)
                        {
                            arret.ArrowheadEnd = ArrowLine.ArrowheadEndEnum.Both;
                            tmatrice[j1] = 0;
                            CalculLignePosition(ref xl1, ref yl1, ref xl2, ref yl2, 29, true);
                        }
                        else 
                        {
                            CalculLignePosition(ref xl1, ref yl1, ref xl2, ref yl2, 29, false);
                        }

                        arret.X1 = xl1;
                        arret.Y1 = yl1;
                        arret.X2 = xl2;
                        arret.Y2 = yl2;

                        arrets.Add(arret);
                    }
                    else if (tmatrice.ElementAt<UInt16>(j1) == 1)
                    {
                        double xl1 = (double)sommets.ElementAt<Label>(i1).GetValue(Canvas.LeftProperty) + 25;
                        double yl1 = (double)sommets.ElementAt<Label>(i1).GetValue(Canvas.TopProperty) + 25;
                        double xl2 = (double)sommets.ElementAt<Label>(j2).GetValue(Canvas.LeftProperty) + 25;
                        double yl2 = (double)sommets.ElementAt<Label>(j2).GetValue(Canvas.TopProperty) + 25;

                        ArrowLine arret = new ArrowLine()
                        {
                            Height = size,
                            Width = size,
                            Stroke = Brushes.Black,
                            StrokeThickness = 3,
                            IsArrowheadClosed = true,
                            ArrowheadEnd = ArrowLine.ArrowheadEndEnum.Start,
                            ArrowheadSizeX = 4,
                            ArrowheadSizeY = 3
                        };

                        CalculLignePosition(ref xl2, ref yl2, ref xl1, ref yl1, 29, false);

                        arret.X1 = xl1;
                        arret.Y1 = yl1;
                        arret.X2 = xl2;
                        arret.Y2 = yl2;

                        arrets.Add(arret);

                    }
                }
            }
        }

        public static void CalculLignePosition(ref Double x1, ref Double y1, ref Double x2, ref Double y2, UInt16 r, Boolean AvecFlech)
        {
            // Pour design les ligne bien
            Double dx = Math.Abs(x1 - x2);
            Double dy = Math.Abs(y1 - y2);
            Double d = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));

            Double xx1 = 0, xx2 = 0, yy1 = 0, yy2 = 0;

            if (!AvecFlech)
            {
                if ((x1 <= x2) && (y1 <= y2))
                {
                    xx1 = x1 + (r * dx / Convert.ToInt32(d)) - 5;
                    yy1 = y1 + (r * dy / Convert.ToInt32(d)) - 5;
                    xx2 = x2 - (r * dx / Convert.ToInt32(d));
                    yy2 = y2 - (r * dy / Convert.ToInt32(d));
                }

                else if ((x1 <= x2) && (y1 >= y2))
                {
                    xx1 = x1 + (r * dx / Convert.ToInt32(d)) - 5;
                    yy1 = y1 - (r * dy / Convert.ToInt32(d)) + 5;
                    xx2 = x2 - (r * dx / Convert.ToInt32(d));
                    yy2 = y2 + (r * dy / Convert.ToInt32(d));
                }

                else if ((x1 >= x2) && (y1 >= y2))
                {
                    xx1 = x1 - (r * dx / Convert.ToInt32(d)) + 5;
                    yy1 = y1 - (r * dy / Convert.ToInt32(d)) + 5;
                    xx2 = x2 + (r * dx / Convert.ToInt32(d));
                    yy2 = y2 + (r * dy / Convert.ToInt32(d));
                }

                else if ((x1 >= x2) && (y1 <= y2))
                {
                    xx1 = x1 - (r * dx / Convert.ToInt32(d)) + 5;
                    yy1 = y1 + (r * dy / Convert.ToInt32(d)) - 5;
                    xx2 = x2 + (r * dx / Convert.ToInt32(d));
                    yy2 = y2 - (r * dy / Convert.ToInt32(d));
                }
            }
            else
            {
                if ((x1 <= x2) && (y1 <= y2))
                {
                    xx1 = x1 + (r * dx / Convert.ToInt32(d));
                    yy1 = y1 + (r * dy / Convert.ToInt32(d));
                    xx2 = x2 - (r * dx / Convert.ToInt32(d));
                    yy2 = y2 - (r * dy / Convert.ToInt32(d));
                }

                else if ((x1 <= x2) && (y1 >= y2))
                {
                    xx1 = x1 + (r * dx / Convert.ToInt32(d));
                    yy1 = y1 - (r * dy / Convert.ToInt32(d));
                    xx2 = x2 - (r * dx / Convert.ToInt32(d));
                    yy2 = y2 + (r * dy / Convert.ToInt32(d));
                }

                else if ((x1 >= x2) && (y1 >= y2))
                {
                    xx1 = x1 - (r * dx / Convert.ToInt32(d));
                    yy1 = y1 - (r * dy / Convert.ToInt32(d));
                    xx2 = x2 + (r * dx / Convert.ToInt32(d));
                    yy2 = y2 + (r * dy / Convert.ToInt32(d));
                }

                else if ((x1 >= x2) && (y1 <= y2))
                {
                    xx1 = x1 - (r * dx / Convert.ToInt32(d));
                    yy1 = y1 + (r * dy / Convert.ToInt32(d));
                    xx2 = x2 + (r * dx / Convert.ToInt32(d));
                    yy2 = y2 - (r * dy / Convert.ToInt32(d));
                }
            }

            x1 = xx1; x2 = xx2; y1 = yy1; y2 = yy2;
        }

        public static void ZoomGraphe(ref UIElementCollection graphe, Double zoom, ref Double size)
        {
            UInt16 indice = 0;
            UInt16 c = Convert.ToUInt16(graphe.Count);
            for (int i = 0; i < c; i++)
            {
                if (graphe[i].GetType() == typeof(Label))
                {
                    graphe[i].SetValue(Label.FontSizeProperty, (Double)graphe[i].GetValue(Label.FontSizeProperty) * zoom);
                    graphe[i].SetValue(Label.WidthProperty, (Double)graphe[i].GetValue(Label.WidthProperty) * zoom);
                    graphe[i].SetValue(Label.HeightProperty, (Double)graphe[i].GetValue(Label.HeightProperty) * zoom);


                    Canvas.SetTop(graphe[i], Canvas.GetTop(graphe[i]) * zoom);
                    Canvas.SetLeft(graphe[i], Canvas.GetLeft(graphe[i]) * zoom);

                    if (indice == 0)
                    {
                        size = Canvas.GetLeft(graphe[i]) * 2;
                    }
                }
                else if (graphe[i].GetType() == typeof(Line))
                {
                    graphe[i].SetValue(Line.X1Property, (Double)graphe[i].GetValue(Line.X1Property) * zoom);
                    graphe[i].SetValue(Line.X2Property, (Double)graphe[i].GetValue(Line.X2Property) * zoom);
                    graphe[i].SetValue(Line.Y1Property, (Double)graphe[i].GetValue(Line.Y1Property) * zoom);
                    graphe[i].SetValue(Line.Y2Property, (Double)graphe[i].GetValue(Line.Y2Property) * zoom);

                }
            }
        }

        public static void ColorerSommets(ref List<Label> sommets, Brush color)
        {
            foreach (Label i in sommets)
            {
                i.Background = color;
            }
        }

        public static void StylerSommets(ref List<Label> sommets, String template)
        {
            foreach (Label i in sommets)
            {
                i.SetResourceReference(Window.TemplateProperty, template);
            }
        }
    }
}
